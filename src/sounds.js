var soundSprite = {
    "alarm": [],
    "bet": [],
    "buttonclick": [],
    "call": [],
    "check": [],
    "deal": [],
    "fold": [],
    "givecards": [],
    "movemoney": [],
    "optionclick": [],
    "optionset": [],
    "raise": [],
    "rotateseat": [],
    "sliderscroll": [],
    "snap": [],
    "takeseat": [],
    "timewarning": [],
    "winanimationsound": [],
    "showstones": [],
    "dealstone": [],
    "dealcard": [],
    "winmoney": [],
    "sliderclick1": [],
    "sliderclick2": [],
    "sliderclick3": [],
    "sliderclick4": [],
    "sliderclick5": [],
    "sliderclick6": [],
    "sliderclick7": [],
    "sliderclick8": [],
    "sliderclick9": []
};

function playSound(soundName, offset, duration) {

    if( offset && duration ) {
        soundSprite[soundName][0] = parseInt(offset);
        soundSprite[soundName][1] = parseInt(duration);
        var sounds = new Howl({
            src: ['data/mixdown.mp3'],
            sprite: soundSprite
        });

        // Play it babe..!
        sounds.play( soundName );
    }
}

var app = angular.module("myApp", []);
app.controller("myCtrl", function($scope) {

    $scope.sounds = Object.keys(soundSprite);

    $scope.offsets = {
        "alarm": 44,
        "bet": 377,
        "buttonclick": 797,
        "call": 1108,
        "check": 1580,
        "deal": 1920,
        "fold": 2510,
        "givecards": 3050,
        "movemoney": 4030,
        "optionclick": 4792,
        "optionset": 4792,
        "raise": 5562,
        "rotateseat": 6188,
        "sliderscroll": 6500,
        "snap": 6812,
        "takeseat": 7188,
        "timewarning": 7768,
        "winmoney": 9750,
        "showstones": 9083,
        "dealstone": 9343,
        "dealcard": 9458,
        "winanimationsound": 8146,
        "sliderclick1": 10404,
        "sliderclick2": 10475,
        "sliderclick3": 10544,
        "sliderclick4": 10609,
        "sliderclick5": 10691,
        "sliderclick6": 10812,
        "sliderclick7": 10908,
        "sliderclick8": 11021,
        "sliderclick9": 11199
    };

    $scope.durations = {
        "alarm": 100,
        "bet": 191,
        "buttonclick": 53,
        "call": 130,
        "check": 280,
        "deal": 480,
        "fold": 110,
        "givecards": 260,
        "movemoney": 420,
        "optionclick": 62,
        "optionset": 150,
        "raise": 327,
        "rotateseat": 62,
        "sliderscroll": 62,
        "snap": 164,
        "takeseat": 270,
        "timewarning": 294,
        "winmoney": 625,
        "showstones": 63,
        "dealstone": 63,
        "dealcard": 125,
        "winanimationsound": 916,
        "sliderclick1": 25,
        "sliderclick2": 27,
        "sliderclick3": 27,
        "sliderclick4": 27,
        "sliderclick5": 74,
        "sliderclick6": 42,
        "sliderclick7": 64,
        "sliderclick8": 48,
        "sliderclick9": 66
    };

    $scope.play = function( soundName ) {
        playSound( soundName, $scope.offsets[soundName], $scope.durations[soundName] );
    };
});